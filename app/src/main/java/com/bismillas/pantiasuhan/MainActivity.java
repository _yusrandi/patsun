package com.bismillas.pantiasuhan;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;

import com.bismillas.pantiasuhan.fragment.HomeFragment;
import com.bismillas.pantiasuhan.fragment.ListFragment;
import com.bismillas.pantiasuhan.fragment.MapsFragment;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

public class MainActivity extends AppCompatActivity implements OnTabSelectListener {

    private BottomBar bottomBar;
    private NestedScrollView mScrollView;
    private GoogleMap mMap;
    MapView mapView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bottomBar = (BottomBar) findViewById(R.id.bottomBar);
        bottomBar.setOnTabSelectListener(this);


    }



    @Override
    public void onTabSelected(int tabId) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch (tabId){
            case R.id.tab_home:
                System.out.println("tab dash klik");
                HomeFragment homeFragment = new HomeFragment();
                fragmentTransaction.replace(R.id.contentContainer,homeFragment);
                fragmentTransaction.commit();
                break;
            case R.id.tab_list:
                ListFragment listFragment = new ListFragment();
                fragmentTransaction.replace(R.id.contentContainer,listFragment);
                fragmentTransaction.commit();
                break;
            case R.id.tab_maps:
                MapsFragment mapsFragment = new MapsFragment();
                fragmentTransaction.replace(R.id.contentContainer,mapsFragment);
                fragmentTransaction.commit();
                break;
        }
    }
}
