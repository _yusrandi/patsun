package com.bismillas.pantiasuhan.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bismillas.pantiasuhan.MySingleton;
import com.bismillas.pantiasuhan.R;
import com.bismillas.pantiasuhan.model.dataPanti;
import com.bismillas.pantiasuhan.utility.urlConfig;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static java.lang.String.valueOf;

public class MapsFragment extends Fragment implements OnMapReadyCallback {
    private final String TAG = "Fragment Maps";
    private View mView;
    private MapView mapView;
    private boolean mapsSupported = true;
    Double lat;
    Double lang;

    LatLng latLng;
    Marker currLocationMarker;
    public static double latPos;
    public static double longPos;
    private double toLatPos, toLongPos;
    Dialog dialog;
    ProgressDialog loading;

    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;

    public Boolean mLocationPermissionsGranted = false;
    private GoogleMap mMap;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private String url = urlConfig.URL_PANTI;
    private ArrayList<dataPanti> listPanti = new ArrayList<>();
    private int i;

    LinearLayout loadinglayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_maps, container, false);

        loadinglayout = mView.findViewById(R.id.layout_loading);
        return mView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        mapView = mView.findViewById(R.id.maps);
        if (mapView != null) {
            mapView.onCreate(null);
            mapView.onResume();
            mapView.getMapAsync(this);
        }
        loading = ProgressDialog.show(getContext(), "Loading data", "Please wait...", false, false);
        getLocationPermission();


    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        Log.d(TAG, "onMap Ready");
        this.mMap = googleMap;

        if (mLocationPermissionsGranted) {
            loading.dismiss();
            getDeviceLocation();
//            parseDataPanti();

            if (listPanti != null) {
                listPanti = HomeFragment.listPanti;
                addMarker();
            }

            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(),
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                return;
            }
            mMap.setMyLocationEnabled(true);
            mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                @Override
                public void onInfoWindowClick(Marker marker) {
                    LatLng latLng = marker.getPosition();

                    for (dataPanti name : listPanti) {
                        if (latLng.longitude == name.getLongitude()) {
                            System.out.println("on map ready " + name.getNamaPanti());
                            dataPanti d = new dataPanti(
                                    name.getId(),
                                    name.getNamaPanti(),
                                    name.getAlamatPanti(),
                                    name.getPenanggungJawab(),
                                    name.getKepalaPanti(),
                                    name.getGambar(),
                                    name.getjPegawai(),
                                    name.getjJiwa(),
                                    name.getKontak(),
                                    name.getDonaturTetap(),
                                    name.getInventory(),
                                    name.getDeskripsi(),
                                    name.getLaki(),
                                    name.getPerempuan(),
                                    name.getSd(),
                                    name.getSmp(),
                                    name.getSma(),
                                    name.getKuliah(),
                                    name.getTidak(),
                                    name.getLatitude(),
                                    name.getLongitude()

                            );
                            getDetailDialog(d);
                        }
                    }
                }
            });

        }

    }

    private void getDetailDialog(final dataPanti data) {

        Dialog dialog = new Dialog(getContext());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.detail_dialog);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);

        final TextView tvNama, tvAlamat, tvPJ, tvKP, tvKontak, tvInventory, tvDeskripsi, tvDonatur, tvLk, tvPr, tvSd, tvSmp, tvSma, tvKuliah, tvTdk;
        ImageView img;
        Button btnGo;


        img = dialog.findViewById(R.id.my_image_list);
        tvNama = dialog.findViewById(R.id.my_tv_nama_panti_dialog);
        tvAlamat = dialog.findViewById(R.id.my_tv_kategori_dialog);
        tvPJ = dialog.findViewById(R.id.my_tv_penanggungjawab_dialog);
        tvKP = dialog.findViewById(R.id.my_tv_kepalapanti_dialog);
        tvKontak = dialog.findViewById(R.id.my_tv_kontak_dialog);
        tvInventory = dialog.findViewById(R.id.my_tv_inventory_dialog);
        tvDeskripsi = dialog.findViewById(R.id.my_tv_deskripsi_dialog);
        btnGo = dialog.findViewById(R.id.my_btn_go_dialog);
        tvDonatur = dialog.findViewById(R.id.my_tv_donatur_dialog);

        tvLk = dialog.findViewById(R.id.my_tv_lk_dialog);
        tvPr = dialog.findViewById(R.id.my_tv_pr_dialog);
        tvSd = dialog.findViewById(R.id.my_tv_sd_dialog);
        tvSmp = dialog.findViewById(R.id.my_tv_smp_dialog);
        tvSma = dialog.findViewById(R.id.my_tv_sma_dialog);
        tvKuliah = dialog.findViewById(R.id.my_tv_kuliah_dialog);
        tvTdk = dialog.findViewById(R.id.my_tv_tdk_dialog);

        tvAlamat.setText(data.getAlamatPanti());
        tvLk.setText(data.getLaki() + " Orang");
        tvPr.setText(data.getPerempuan() + " Orang");
        tvSd.setText(data.getSd() + " Orang");
        tvSmp.setText(data.getSmp() + " Orang");
        tvSma.setText(data.getSma() + " Orang");
        tvKuliah.setText(data.getKuliah() + " Orang");
        tvTdk.setText(data.getTidak() + " Orang");

        tvNama.setText(data.getNamaPanti());
        tvPJ.setText(data.getPenanggungJawab());
        tvKP.setText(data.getKepalaPanti());
        tvKontak.setText(data.getKontak());
        tvInventory.setText(data.getInventory());
        tvDeskripsi.setText(data.getDeskripsi());
        tvDonatur.setText(data.getDonaturTetap());

        Picasso.with(getContext())
                .load(urlConfig.URL_GAMBAR + data.getGambar())
                .placeholder(R.drawable.background)
                .error(android.R.drawable.stat_notify_error)
                .into(img);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showNavigation(data.getLatitude(), data.getLongitude());
            }
        });


        dialog.show();
    }

    public void showNavigation(Double latitude, Double langitude) {

        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                        "&saddr=" + latPos + "," + longPos +
                        "&daddr=" + latitude + "," + langitude +
                        "&hl=zh&t=m&dirflg=d"
                ));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
        getContext().startActivity(intent);

    }

    public void getLocationPermission() {
        Log.d(TAG, "getLocationPermisson : getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if (ContextCompat.checkSelfPermission(this.getContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

            if (ContextCompat.checkSelfPermission(this.getContext(), COURSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                mLocationPermissionsGranted = true;
            } else {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
            }
        } else {
            ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }

    public void getDeviceLocation() {
        Log.d(TAG, "getDeviceLocation : getting device current location");

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        try {

            if (mLocationPermissionsGranted) {
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if (task.isSuccessful() && task.getResult() != null) {
                            Log.d(TAG, "onComplete : found location");

                            Location currentLocation = (Location) task.getResult();
                            moveCamera(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM);


                        } else {
                            Log.d(TAG, "onComplete : current location is null");
                            Toast.makeText(getContext(), "Unable tu get current location ", Toast.LENGTH_SHORT).show();


                        }
                    }
                });
            }
        } catch (SecurityException e) {
            Log.d(TAG, "getDeviceLocation : SecurityException " + e);
        }
    }

    private void moveCamera(LatLng latLng, float zoom) {
        Log.d(TAG, "moveCamera  : moving camera to lat " + latLng.latitude + " and lng " + latLng.longitude);

        latPos = latLng.latitude;
        longPos = latLng.longitude;

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);

        markerOptions.title("Current Position");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        currLocationMarker = mMap.addMarker(markerOptions);

    }

    private void addMarker() {
        if (listPanti != null) {
            for (dataPanti name : listPanti) {
                mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(name.getLatitude()), Double.valueOf(name.getLongitude()))).title(name.getNamaPanti()).snippet(name.getAlamatPanti()).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

            }
        }
    }
//    private void parseDataPanti() {
//        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.d(TAG, response);
//                System.out.println(response);
//                loadinglayout.setVisibility(View.GONE);
//                try {
//
//                    JSONArray data = new JSONArray(response);
//
//                    for (i = 0; i < data.length(); i++) {
//                        final JSONObject jo = data.getJSONObject(i);
//
//                        listPanti.add(new dataPanti(jo.getString("id_panti"), jo.getString("Nama"), jo.getString("Penanggung_Jawab"), jo.getString("Kepala_Panti"), jo.getString("Gambar"), jo.getString("Jum_pegawai"),jo.getString("Jum_jiwa"),jo.getString("Kontak"),jo.getString("Donatur_Tetap"),jo.getString("Inventory"),jo.getString("Deskripsi"),jo.getString("kategori"), Double.parseDouble(jo.getString("latitude")), Double.parseDouble(jo.getString("longitude"))));
//                        if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Anak Terlantar")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
//                        }if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Penyandang Cacat")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//                        }if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Lansia Terlantar")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//                        }
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, error.toString());
//                Toast.makeText(getContext(), "Something went wrong."+error, Toast.LENGTH_LONG).show();
//                System.out.println("ini errorx"+error);
//            }
//        });
//
//        MySingleton.getInstance(getContext()).addToRequestQueue(request);
//    }

//    private void parseDataPanti(String url) {
//        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Log.d(TAG, response);
//                System.out.println(response);
//                loadinglayout.setVisibility(View.GONE);
//                mMap.clear();
//                try {
//
//                    JSONArray data = new JSONArray(response);
//
//
//                    for (i = 0; i < data.length(); i++) {
//                        final JSONObject jo = data.getJSONObject(i);
//
//                        listPanti.add(new dataPanti(jo.getString("id_panti"), jo.getString("Nama"), jo.getString("Penanggung_Jawab"), jo.getString("Kepala_Panti"), jo.getString("Gambar"), jo.getString("Jum_pegawai"),jo.getString("Jum_jiwa"),jo.getString("Kontak"),jo.getString("Donatur_Tetap"),jo.getString("Inventory"),jo.getString("Deskripsi"),jo.getString("kategori"), Double.parseDouble(jo.getString("latitude")), Double.parseDouble(jo.getString("longitude"))));
//                        if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Anak Terlantar")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
//                        }if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Penyandang Cacat")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
//                        }if(jo.getString("kategori").equalsIgnoreCase("Panti Asuhan Lansia Terlantar")){
//                            mMap.addMarker(new MarkerOptions().position(new LatLng(Double.valueOf(jo.getString("latitude")), Double.valueOf(jo.getString("longitude")))).title(jo.getString("Nama")).snippet(jo.getString("kategori")).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE)));
//                        }
//
//                    }
//
//
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d(TAG, error.toString());
//                Toast.makeText(getContext(), "Something went wrong."+error, Toast.LENGTH_LONG).show();
//                System.out.println("ini errorx"+error);
//            }
//        });
//
//        MySingleton.getInstance(getContext()).addToRequestQueue(request);
//    }


}
