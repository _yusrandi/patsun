package com.bismillas.pantiasuhan.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bismillas.pantiasuhan.MySingleton;
import com.bismillas.pantiasuhan.R;
import com.bismillas.pantiasuhan.adapter.PantiAdapter;
import com.bismillas.pantiasuhan.model.dataPanti;
import com.bismillas.pantiasuhan.utility.urlConfig;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class ListFragment extends Fragment {
    private final String TAG = "MainActivity";
    private View mView;
    private ArrayList<dataPanti> listPanti = new ArrayList<>();
    private String url = urlConfig.URL_PANTI;
    private RecyclerView recyclerView;

    LinearLayout loading;


    public ListFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_list, container, false);

        recyclerView = mView.findViewById(R.id.rvList);
        recyclerView.setHasFixedSize(true);
        recyclerView.setNestedScrollingEnabled(false);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(manager);

        if(HomeFragment.listPanti!= null){
            listPanti = HomeFragment.listPanti;
        }
        PantiAdapter adapter = new PantiAdapter(getContext(), listPanti, recyclerView);
        recyclerView.setAdapter(adapter);


//        loading = mView.findViewById(R.id.layout_loading);

        return mView;
    }




}
