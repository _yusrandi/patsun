package com.bismillas.pantiasuhan.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.net.Uri;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bismillas.pantiasuhan.MainActivity;
import com.bismillas.pantiasuhan.MySingleton;
import com.bismillas.pantiasuhan.R;
import com.bismillas.pantiasuhan.SplashActivity;
import com.bismillas.pantiasuhan.adapter.PantiAdapter;
import com.bismillas.pantiasuhan.model.dataPanti;
import com.bismillas.pantiasuhan.utility.urlConfig;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.maps.android.SphericalUtil;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static java.lang.String.valueOf;


public class HomeFragment extends Fragment{
    private final String TAG = "HomeFragment";
    private View mView;
    private SliderLayout sliderSlide;

    public static ArrayList<dataPanti> listPanti = new ArrayList<>();
    private String url = urlConfig.URL_PANTI;

    LinearLayout loading, loadingTerdekat;

    public static double latPos , longPos;

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COURSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 1234;
    private static final float DEFAULT_ZOOM = 15f;
    public Boolean mLocationPermissionsGranted = SplashActivity.mLocationPermissionsGranted;
    private FusedLocationProviderClient mFusedLocationProviderClient;

    private MapView mapView;

    private ImageView imgTerdekat;
    TextView tvNamaTerdekat, tvKategoriTerdekat, tvAlamatTerdekat;

    dataPanti DATA;
    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_home, container, false);

        sliderSlide = (SliderLayout) mView.findViewById(R.id.slider_iklan);
        loading = mView.findViewById(R.id.layout_loading);
        loadingTerdekat = mView.findViewById(R.id.loading);

        mapView = mView.findViewById(R.id.maps_terdekat);

        imgTerdekat = mView.findViewById(R.id.img_terdekat);
        tvNamaTerdekat = mView.findViewById(R.id.my_tv_nama_panti_terdekat);
        tvKategoriTerdekat = mView.findViewById(R.id.my_tv_kategori_terdekat);

        loadingTerdekat.setVisibility(View.VISIBLE);

        parseDataPanti();

        getLocationPermission();

        getDeviceLocation();

        return mView;

    }
    public void getLocationPermission(){
        Log.d(TAG, "getLocationPermisson : getting location permissions");
        String[] permissions = {Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(this.getContext(), FINE_LOCATION)==PackageManager.PERMISSION_GRANTED){

            if(ContextCompat.checkSelfPermission(this.getContext(), COURSE_LOCATION)==PackageManager.PERMISSION_GRANTED){
                mLocationPermissionsGranted = true;
            }else {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
            }
        }else {
            ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSIONS_REQUEST_CODE);
        }
    }
    public void getDeviceLocation(){
        Log.d(TAG, "getDeviceLocation : getting device current location");

        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(getActivity());
        try {

            if(mLocationPermissionsGranted){
                Task location = mFusedLocationProviderClient.getLastLocation();
                location.addOnCompleteListener(new OnCompleteListener() {
                    @Override
                    public void onComplete(@NonNull Task task) {
                        if(task.isSuccessful() && task.getResult() != null){
                            Log.d(TAG, "onComplete : found location");

                            Location currentLocation = (Location) task.getResult();
                            latPos = currentLocation.getLatitude();
                            longPos = currentLocation.getLongitude();


                            Log.d(TAG, "onCreateView : lat "+latPos);
//                            parseDataPanti();


                        }else{
                            Log.d(TAG, "onComplete : current location is null");
                            Toast.makeText(getContext(), "Unable tu get current location ", Toast.LENGTH_SHORT).show();


                        }
                    }
                });
            }
        }catch (SecurityException e){
            Log.d(TAG, "getDeviceLocation : SecurityException "+e);
        }
    }

    private void parseDataPanti() {
        listPanti.clear();
        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d(TAG, response);
                System.out.println(response);
                loading.setVisibility(View.GONE);


               try {

                    final JSONArray data = new JSONArray(response);

                    for (int i = 0; i < data.length(); i++) {
                        JSONObject jo = data.getJSONObject(i);

                        listPanti.add(new dataPanti(
                                jo.getString("id_panti"),
                                jo.getString("Nama"),
                                jo.getString("alamat"),
                                jo.getString("Penanggung_Jawab"),
                                jo.getString("Kepala_Panti"),
                                jo.getString("Gambar"),
                                jo.getString("Jum_pegawai"),
                                jo.getString("Jum_jiwa"),
                                jo.getString("Kontak"),
                                jo.getString("Donatur_Tetap"),
                                jo.getString("Inventory"),
                                jo.getString("Deskripsi"),
                                jo.getString("laki_laki"),
                                jo.getString("perempuan"),
                                jo.getString("sd"),
                                jo.getString("smp"),
                                jo.getString("sma"),
                                jo.getString("kuliah"),
                                jo.getString("tidak"),
                                jo.getDouble("latitude"),
                                jo.getDouble("longitude")
                        ));

                    }

                    for (final dataPanti name : listPanti) {
                        TextSliderView textSliderView = new TextSliderView(getContext());
                        // initialize a SliderLayout
                        textSliderView
                                .description(name.getNamaPanti())
                                .image(urlConfig.URL_GAMBAR+name.getGambar())
                                .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                    @Override
                                    public void onSliderClick(BaseSliderView slider) {

                                        dataPanti d = new dataPanti(
                                                name.getId(),
                                                name.getNamaPanti(),
                                                name.getAlamatPanti(),
                                                name.getPenanggungJawab(),
                                                name.getKepalaPanti(),
                                                name.getGambar(),
                                                name.getjPegawai(),
                                                name.getjJiwa(),
                                                name.getKontak(),
                                                name.getDonaturTetap(),
                                                name.getInventory(),
                                                name.getDeskripsi(),
                                                name.getLaki(),
                                                name.getPerempuan(),
                                                name.getSd(),
                                                name.getSmp(),
                                                name.getSma(),
                                                name.getKuliah(),
                                                name.getTidak(),
                                                name.getLatitude(),
                                                name.getLongitude()
                                        );
                                        getDetailDialog(d);
                                    }
                                })
                                .setScaleType(BaseSliderView.ScaleType.Fit);

                        //add your extra information
//                        textSliderView.bundle(new Bundle());
//                        textSliderView.getBundle()
//                                .putString("extra",name.getNama());

                        sliderSlide.addSlider(textSliderView);
                    }
                    sliderSlide.setPresetTransformer(SliderLayout.Transformer.Accordion);
                    sliderSlide.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
//                    sliderIklan.setCustomAnimation(new DescriptionAnimation());
                    sliderSlide.setDuration(3000);

                    getLokasiTerdekat();

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, error.toString());
                Toast.makeText(getContext(), "Something went wrong."+error, Toast.LENGTH_LONG).show();
                System.out.println("ini errorx"+error);
            }
        });

        MySingleton.getInstance(getContext()).addToRequestQueue(request);
    }


    private void getLokasiTerdekat(){
        loadingTerdekat.setVisibility(View.GONE);
        DecimalFormat df = new DecimalFormat("#.#");

        ArrayList<dataPanti> listTerdekat = new ArrayList<>();
        ArrayList<Double> jarakTerdekat = new ArrayList<>();


        double terdekat = 0, oriLat=0, oriLong=0, Distance;

        for (dataPanti name : listPanti){
            System.out.println("Latitude Panti : "+name.getLatitude());
            Log.d(TAG, "ListPanti : "+name.getNamaPanti());
            oriLat = name.getLatitude();
            oriLong = name.getLongitude();
            LatLng from = new LatLng(latPos,longPos);
            LatLng to = new LatLng(oriLat,oriLong);

            Double distance = SphericalUtil.computeDistanceBetween(from, to); // utility dari maps untuk menghitung jarak
            Distance = (distance*0.003)/2;

            Log.d(TAG, String.valueOf(Distance));

            dataPanti d = new dataPanti(
                    name.getId(),
                    name.getNamaPanti(),
                    name.getAlamatPanti(),
                    name.getPenanggungJawab(),
                    name.getKepalaPanti(),
                    name.getGambar(),
                    name.getjPegawai(),
                    name.getjJiwa(),
                    name.getKontak(),
                    name.getDonaturTetap(),
                    name.getInventory(),
                    name.getDeskripsi(),
                    name.getLaki(),
                    name.getPerempuan(),
                    name.getSd(),
                    name.getSmp(),
                    name.getSma(),
                    name.getKuliah(),
                    name.getTidak(),
                    name.getLatitude(),
                    name.getLongitude(),
                    Distance
            );
            listTerdekat.add(d);
            jarakTerdekat.add(Distance);

        }


        Collections.sort(jarakTerdekat);

        for (final dataPanti dataPanti :listTerdekat){
            if(jarakTerdekat.get(0)==dataPanti.getDistance()){

                Log.d(TAG, "getLokasiTerdekat Jarak Terdekat "+dataPanti.getDistance());
                Log.d(TAG, "getLokasiTerdekat nama Panti "+dataPanti.getNamaPanti());
                Log.d(TAG, "getLokasiTerdekat gambar "+dataPanti.getGambar());

                tvNamaTerdekat.setText(dataPanti.getNamaPanti());
                tvKategoriTerdekat.setText(dataPanti.getAlamatPanti());

                Picasso.with(getContext())
                        .load(urlConfig.URL_GAMBAR+dataPanti.getGambar())
                        .placeholder(R.drawable.loading)
                        .error(android.R.drawable.stat_notify_error)
                        .into(imgTerdekat);

                imgTerdekat.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getDetailDialog(dataPanti);
                    }
                });

            }
        }


    }

    public void getDetailDialog(final dataPanti data) {

        System.out.println("getDetailDoalog : current location lat = "+latPos+" longpos = "+longPos);

        Dialog dialog = new Dialog(getContext());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.detail_dialog);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);

        final TextView tvNama, tvAlamat, tvPJ, tvKP, tvKontak, tvInventory, tvDeskripsi,  tvDonatur, tvLk, tvPr, tvSd, tvSmp, tvSma, tvKuliah, tvTdk;
        ImageView img;

        Button btnGo;

        img = dialog.findViewById(R.id.my_image_list);
        tvNama = dialog.findViewById(R.id.my_tv_nama_panti_dialog);
        tvAlamat = dialog.findViewById(R.id.my_tv_kategori_dialog);
        tvPJ = dialog.findViewById(R.id.my_tv_penanggungjawab_dialog);
        tvKP = dialog.findViewById(R.id.my_tv_kepalapanti_dialog);
        tvKontak = dialog.findViewById(R.id.my_tv_kontak_dialog);
        tvInventory = dialog.findViewById(R.id.my_tv_inventory_dialog);
        tvDeskripsi = dialog.findViewById(R.id.my_tv_deskripsi_dialog);
        tvDonatur = dialog.findViewById(R.id.my_tv_donatur_dialog);

        tvLk = dialog.findViewById(R.id.my_tv_lk_dialog);
        tvPr = dialog.findViewById(R.id.my_tv_pr_dialog);
        tvSd = dialog.findViewById(R.id.my_tv_sd_dialog);
        tvSmp = dialog.findViewById(R.id.my_tv_smp_dialog);
        tvSma = dialog.findViewById(R.id.my_tv_sma_dialog);
        tvKuliah = dialog.findViewById(R.id.my_tv_kuliah_dialog);
        tvTdk = dialog.findViewById(R.id.my_tv_tdk_dialog);

        tvAlamat.setText(data.getAlamatPanti());
        tvLk.setText(data.getLaki()+" Orang");
        tvPr.setText(data.getPerempuan()+" Orang");
        tvSd.setText(data.getSd()+" Orang");
        tvSmp.setText(data.getSmp()+" Orang");
        tvSma.setText(data.getSma()+" Orang");
        tvKuliah.setText(data.getKuliah()+" Orang");
        tvTdk.setText(data.getTidak()+" Orang");

        btnGo = dialog.findViewById(R.id.my_btn_go_dialog);


        tvNama.setText(data.getNamaPanti());
        tvPJ.setText(data.getPenanggungJawab());
        tvKP.setText(data.getKepalaPanti());
        tvKontak.setText(data.getKontak());
        tvInventory.setText(data.getInventory());
        tvDeskripsi.setText(data.getDeskripsi());

        tvDonatur.setText(data.getDonaturTetap());

        Picasso.with(getContext())
                .load(urlConfig.URL_GAMBAR+data.getGambar())
                .placeholder(R.drawable.loading)
                .error(android.R.drawable.stat_notify_error)

                .into(img);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showNavigation(data.getLatitude(), data.getLongitude());
            }
        });



        dialog.show();

    }
    public void showNavigation(Double latitude, Double langitude) {

        if(latPos==0 || longPos==0){
            Toast.makeText(getContext(), "Please wait, getting your position ! ", Toast.LENGTH_LONG).show();
        }else {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                            "&saddr=" + latPos + "," + longPos +
                            "&daddr=" + latitude + "," + langitude +
                            "&hl=zh&t=m&dirflg=d"
                    ));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            startActivity(intent);
        }


    }
}
