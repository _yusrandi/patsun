package com.bismillas.pantiasuhan.model;

public class dataSekolah {
    private String sd, smp, sma, kuliah, tidak;

    public dataSekolah(String sd, String smp, String sma, String kuliah, String tidak) {
        this.sd = sd;
        this.smp = smp;
        this.sma = sma;
        this.kuliah = kuliah;
        this.tidak = tidak;
    }

    public String getSd() {
        return sd;
    }

    public String getSmp() {
        return smp;
    }

    public String getSma() {
        return sma;
    }

    public String getKuliah() {
        return kuliah;
    }

    public String getTidak() {
        return tidak;
    }
}
