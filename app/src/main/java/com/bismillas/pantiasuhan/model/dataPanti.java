package com.bismillas.pantiasuhan.model;



public class dataPanti {
    private String id, namaPanti, alamatPanti,  penanggungJawab, kepalaPanti, gambar, jPegawai, jJiwa, kontak, donaturTetap, inventory, deskripsi, laki, perempuan, sd, smp, sma, kuliah, tidak;
    private double latitude;
    private double longitude;
    private double Distance;


    public dataPanti(String id, String namaPanti, String alamatPanti, String penanggungJawab, String kepalaPanti, String gambar, String jPegawai, String jJiwa, String kontak, String donaturTetap, String inventory, String deskripsi, String laki, String perempuan, String sd, String smp, String sma, String kuliah, String tidak, double latitude, double longitude) {
        this.id = id;
        this.namaPanti = namaPanti;
        this.alamatPanti = alamatPanti;
        this.penanggungJawab = penanggungJawab;
        this.kepalaPanti = kepalaPanti;
        this.gambar = gambar;
        this.jPegawai = jPegawai;
        this.jJiwa = jJiwa;
        this.kontak = kontak;
        this.donaturTetap = donaturTetap;
        this.inventory = inventory;
        this.deskripsi = deskripsi;
        this.laki = laki;
        this.perempuan = perempuan;
        this.sd = sd;
        this.smp = smp;
        this.sma = sma;
        this.kuliah = kuliah;
        this.tidak = tidak;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public dataPanti(String id, String namaPanti, String alamatPanti, String penanggungJawab, String kepalaPanti, String gambar, String jPegawai, String jJiwa, String kontak, String donaturTetap, String inventory, String deskripsi, String laki, String perempuan, String sd, String smp, String sma, String kuliah, String tidak, double latitude, double longitude, double distance) {
        this.id = id;
        this.namaPanti = namaPanti;
        this.alamatPanti = alamatPanti;
        this.penanggungJawab = penanggungJawab;
        this.kepalaPanti = kepalaPanti;
        this.gambar = gambar;
        this.jPegawai = jPegawai;
        this.jJiwa = jJiwa;
        this.kontak = kontak;
        this.donaturTetap = donaturTetap;
        this.inventory = inventory;
        this.deskripsi = deskripsi;
        this.laki = laki;
        this.perempuan = perempuan;
        this.sd = sd;
        this.smp = smp;
        this.sma = sma;
        this.kuliah = kuliah;
        this.tidak = tidak;
        this.latitude = latitude;
        this.longitude = longitude;
        Distance = distance;
    }

    public double getDistance() {
        return Distance;
    }


    public String getId() {
        return id;
    }

    public String getNamaPanti() {
        return namaPanti;
    }

    public String getPenanggungJawab() {
        return penanggungJawab;
    }

    public String getKepalaPanti() {
        return kepalaPanti;
    }

    public String getGambar() {
        return gambar;
    }

    public String getjPegawai() {
        return jPegawai;
    }

    public String getjJiwa() {
        return jJiwa;
    }

    public String getKontak() {
        return kontak;
    }

    public String getDonaturTetap() {
        return donaturTetap;
    }

    public String getInventory() {
        return inventory;
    }

    public String getDeskripsi() {
        return deskripsi;
    }


    public String getAlamatPanti() {
        return alamatPanti;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getLaki() {
        return laki;
    }

    public String getPerempuan() {
        return perempuan;
    }

    public String getSd() {
        return sd;
    }

    public String getSmp() {
        return smp;
    }

    public String getSma() {
        return sma;
    }

    public String getKuliah() {
        return kuliah;
    }

    public String getTidak() {
        return tidak;
    }
}
