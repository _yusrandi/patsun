package com.bismillas.pantiasuhan.model;

public class dataJumlahJiwa {
    private String laki, perempuan;

    public dataJumlahJiwa(String laki, String perempuan) {
        this.laki = laki;
        this.perempuan = perempuan;
    }

    public String getLaki() {
        return laki;
    }

    public String getPerempuan() {
        return perempuan;
    }
}
