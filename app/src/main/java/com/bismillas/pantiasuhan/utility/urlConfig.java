package com.bismillas.pantiasuhan.utility;



public class urlConfig {

    public final static String URL = "http://bismillahs.com/patsun/admin/";
    public final static String URL_PANTI = URL+"panti_url.php";
    public final static String URL_TERLANTAR = URL+"panti_anak_terlantar_url.php";
    public final static String URL_CACAT = URL+"panti_penyandang_cacat_url.php";
    public final static String URL_LANSIA = URL+"panti_lansia_terlantar_url.php";
    public final static String URL_GAMBAR = URL+"gambar/";
    public final static String URL_JUMLAH_JIWA = URL+"jumlah_jiwa_url.php";
    public final static String URL_JUMLAH_SEKOLAH = URL+"jumlah_sekolah_url.php";

}
