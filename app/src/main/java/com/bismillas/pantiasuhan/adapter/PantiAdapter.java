package com.bismillas.pantiasuhan.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bismillas.pantiasuhan.MySingleton;
import com.bismillas.pantiasuhan.R;
import com.bismillas.pantiasuhan.fragment.HomeFragment;
import com.bismillas.pantiasuhan.fragment.MapsFragment;
import com.bismillas.pantiasuhan.model.dataPanti;
import com.bismillas.pantiasuhan.utility.urlConfig;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.google.android.gms.vision.text.Line;
import com.google.android.gms.vision.text.Text;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PantiAdapter extends RecyclerView.Adapter<PantiAdapter.PantiViewHolder> {

    private Context context;
    private ArrayList<dataPanti> listPanti;
    private RecyclerView recyclerView;
    ProgressDialog loading;
    private double latPos = MapsFragment.latPos;
    private double longPos = MapsFragment.longPos;

    private static final String URL_JUMLAH_JIWA = urlConfig.URL_JUMLAH_JIWA;
    private static String TAG = "PantiAdapter";


    private TextView tvBayi, tvAnak, tvRemaja, tvDewasa, tvLansia;

    public PantiAdapter(Context context, ArrayList<dataPanti> listPanti, RecyclerView recyclerView) {
        this.context = context;
        this.listPanti = listPanti;
        this.recyclerView = recyclerView;


    }


    @Override
    public PantiViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.from(parent.getContext())
                .inflate(R.layout.item_list, parent, false);


        latPos = MapsFragment.latPos;
        longPos = MapsFragment.longPos;

        PantiViewHolder ViewHolder = new PantiViewHolder(view);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = recyclerView.getChildLayoutPosition(view);
//
                dataPanti data = listPanti.get(position);
                getDetailDialog(data);

            }
        });
        return ViewHolder;


    }
    public void getDetailDialog(final dataPanti data) {

        System.out.println("getDetailDoalog : current location lat = "+latPos+" longpos = "+longPos);

        Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.detail_dialog);

        WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        layoutParams.copyFrom(window.getAttributes());
        layoutParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        layoutParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        window.setAttributes(layoutParams);

        final TextView tvNama, tvAlamat, tvPJ, tvKP, tvKontak, tvInventory, tvDeskripsi,  tvDonatur, tvLk, tvPr, tvSd, tvSmp, tvSma, tvKuliah, tvTdk;
        LinearLayout layoutJiwa;
        ImageView img;

        Button btnGo;

        img = dialog.findViewById(R.id.my_image_list);
        tvNama = dialog.findViewById(R.id.my_tv_nama_panti_dialog);
        tvAlamat = dialog.findViewById(R.id.my_tv_kategori_dialog);
        tvPJ = dialog.findViewById(R.id.my_tv_penanggungjawab_dialog);
        tvKP = dialog.findViewById(R.id.my_tv_kepalapanti_dialog);
        tvKontak = dialog.findViewById(R.id.my_tv_kontak_dialog);
        tvInventory = dialog.findViewById(R.id.my_tv_inventory_dialog);
        tvDeskripsi = dialog.findViewById(R.id.my_tv_deskripsi_dialog);
        tvDonatur = dialog.findViewById(R.id.my_tv_donatur_dialog);
        btnGo = dialog.findViewById(R.id.my_btn_go_dialog);


        tvLk = dialog.findViewById(R.id.my_tv_lk_dialog);
        tvPr = dialog.findViewById(R.id.my_tv_pr_dialog);
        tvSd = dialog.findViewById(R.id.my_tv_sd_dialog);
        tvSmp = dialog.findViewById(R.id.my_tv_smp_dialog);
        tvSma = dialog.findViewById(R.id.my_tv_sma_dialog);
        tvKuliah = dialog.findViewById(R.id.my_tv_kuliah_dialog);
        tvTdk = dialog.findViewById(R.id.my_tv_tdk_dialog);

        tvAlamat.setText(data.getAlamatPanti());
        tvLk.setText(data.getLaki()+" Orang");
        tvPr.setText(data.getPerempuan()+" Orang");
        tvSd.setText(data.getSd()+" Orang");
        tvSmp.setText(data.getSmp()+" Orang");
        tvSma.setText(data.getSma()+" Orang");
        tvKuliah.setText(data.getKuliah()+" Orang");
        tvTdk.setText(data.getTidak()+" Orang");



        tvNama.setText(data.getNamaPanti());
        tvPJ.setText(data.getPenanggungJawab());
        tvKP.setText(data.getKepalaPanti());
        tvKontak.setText(data.getKontak());
        tvInventory.setText(data.getInventory());
        tvDeskripsi.setText(data.getDeskripsi());
        tvDonatur.setText(data.getDonaturTetap());

        Picasso.with(context)
                .load(urlConfig.URL_GAMBAR+data.getGambar())
                .placeholder(R.drawable.loading)
                .error(android.R.drawable.stat_notify_error)

                .into(img);

        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showNavigation(data.getLatitude(), data.getLongitude());
            }
        });



        dialog.show();

    }

    public void showNavigation(Double latitude, Double langitude) {

        if(latPos==0 || longPos==0){
            Toast.makeText(context, "Please wait, getting your position ! ", Toast.LENGTH_LONG).show();
        }else {
            Intent intent = new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://ditu.google.cn/maps?f=d&source=s_d" +
                            "&saddr=" + HomeFragment.latPos + "," + HomeFragment.longPos +
                            "&daddr=" + latitude + "," + langitude +
                            "&hl=zh&t=m&dirflg=d"
                    ));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK & Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
            intent.setClassName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity");
            context.startActivity(intent);
        }
    }
    @Override
    public void onBindViewHolder(PantiViewHolder holder, int position) {


        final dataPanti data = listPanti.get(position);

        holder.tvNama.setText(data.getNamaPanti());
        holder.tvAlamat.setText(data.getAlamatPanti());

        Picasso.with(context)
                .load(urlConfig.URL_GAMBAR+data.getGambar())
                .placeholder(R.drawable.loading)
                .error(android.R.drawable.stat_notify_error)
                .into(holder.img);
    }


    @Override
    public int getItemCount() {
        if (listPanti != null) {
            return listPanti.size();
        }
        return 0;
    }




    //ViewHolder class
    public static class PantiViewHolder extends RecyclerView.ViewHolder {



        public TextView tvNama, tvAlamat;
        public ImageView img;

        public PantiViewHolder(View itemView) {
            super(itemView);

            tvNama = itemView.findViewById(R.id.my_tv_nama_panti_list);
            tvAlamat = itemView.findViewById(R.id.my_tv_alamat_list);
            img = itemView.findViewById(R.id.my_image_list);
        }
    }
}
